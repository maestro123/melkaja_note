package yurlova.notebook;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import yurlova.notebook.core.model.LocalNote;

import static org.junit.Assert.assertEquals;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("yurlova.notebook", appContext.getPackageName());
    }

    @Test
    public void testLocalNoteCreate() {
        LocalNote note = new LocalNote();
        note.setTitle("testTitle");
        note.setContent("testContent");

        note.save(InstrumentationRegistry.getTargetContext());
    }

}
