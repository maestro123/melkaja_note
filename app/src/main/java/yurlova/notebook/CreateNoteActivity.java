package yurlova.notebook;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;

import com.thebluealliance.spectrum.SpectrumDialog;

import java.io.Serializable;

import yurlova.notebook.core.INote;
import yurlova.notebook.core.NotesProvider;
import yurlova.notebook.core.model.DbNote;
import yurlova.notebook.core.model.LocalNote;

public class CreateNoteActivity extends AppCompatActivity {

    private static final String PARAM_KEY = "key";
    private static final String SAVED_COLOR = "savedColor";
    private static final String PALETTE_DIALOG_TAG = "PaletteDialog";

    public static void start(Context context, Serializable key) {
        Intent intent = new Intent(context, CreateNoteActivity.class);
        intent.putExtra(PARAM_KEY, key);
        context.startActivity(intent);
    }

    private static final int[] PALETTE_COLORS = new int[]{
            Color.parseColor("#2196F3"),
            Color.parseColor("#3F51B5"),
            Color.parseColor("#F44336"),
            Color.parseColor("#673AB7"),
            Color.parseColor("#009688"),
            Color.parseColor("#9C27B0"),
            Color.parseColor("#FFEB3B"),
            Color.parseColor("#FF9800"),
            Color.parseColor("#4CAF50"),
            Color.parseColor("#795548"),
            Color.parseColor("#607D8B"),
            Color.parseColor("#E91E63")
    };

    private EditText edtTitle;
    private EditText edtContent;
    private ImageButton btnBack;
    private ImageButton btnDone;
    private ImageButton btnPalette;

    private INote mNote;

    private String mInitialTitle;
    private String mInitialContent;

    private int mSelectedColor = Color.parseColor("#2196F3");

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_note);

        if (savedInstanceState != null) {
            mSelectedColor = savedInstanceState.getInt(SAVED_COLOR);
            SpectrumDialog spectrumDialog = (SpectrumDialog) getSupportFragmentManager()
                    .findFragmentByTag(PALETTE_DIALOG_TAG);
            if (spectrumDialog != null) {
                spectrumDialog.setOnColorSelectedListener(mColorSelectListener);
            }
        }

        mNote = NotesProvider.Instance().getNote(getIntent().getSerializableExtra(PARAM_KEY));

        edtTitle = (EditText) findViewById(R.id.edt_title);
        edtContent = (EditText) findViewById(R.id.edt_content);
        btnBack = (ImageButton) findViewById(R.id.btn_back);
        btnDone = (ImageButton) findViewById(R.id.btn_done);
        btnPalette = (ImageButton) findViewById(R.id.btn_palette);

        if (mNote != null) {
            mInitialTitle = mNote.getTitle();
            mInitialContent = mNote.getContent();
            if (savedInstanceState == null) {
                mSelectedColor = mNote.getColor();
                edtTitle.setText(mInitialTitle);
                edtContent.setText(mInitialContent);
            }
        }

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final boolean isAnyValid = isAnyValid();
                if (mNote != null) {
                    if (!isAnyValid) {
                        showDeleteConfirmDialog();
                    } else {
                        saveNote(mNote);
                    }
                } else if (!isAnyValid) {
                    finish();
                } else {
                    showSaveOptionDialog();
                }
            }
        });

        btnPalette.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SpectrumDialog.Builder(getApplicationContext())
                        .setColors(PALETTE_COLORS)
                        .setSelectedColor(mSelectedColor)
                        .setOnColorSelectedListener(mColorSelectListener)
                        .build()
                        .show(getSupportFragmentManager(), PALETTE_DIALOG_TAG);
            }
        });

    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(SAVED_COLOR, mSelectedColor);
    }

    @Override
    public void onBackPressed() {
        if (mNote != null) {
            if (!isAnyValid()) {
                showDeleteConfirmDialog();
                return;
            } else {
                saveNote(mNote);
            }
        } else if (isAnyValid()) {
            showExitConfirmDialog();
            return;
        }
        super.onBackPressed();
    }

    private boolean isAnyValid() {
        return isTitleValid() || isContentValid();
    }

    private boolean isTitleValid() {
        return !TextUtils.isEmpty(edtTitle.getText());
    }

    private boolean isContentValid() {
        return !TextUtils.isEmpty(edtContent.getText());
    }

    private void showDeleteConfirmDialog() {
        new AlertDialog.Builder(this)
                .setTitle(R.string.dialog_delete_confirm_title)
                .setMessage(R.string.dialog_delete_confirm_message)
                .setPositiveButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //ignore
                    }
                })
                .setNegativeButton(R.string.delete, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        NotesProvider.Instance().delete(CreateNoteActivity.this, mNote);
                        finish();
                    }
                })
                .show();
    }

    private void showExitConfirmDialog() {
        new AlertDialog.Builder(this)
                .setTitle(R.string.dialog_exit_confirm_title)
                .setMessage(R.string.dialog_exit_confirm_message)
                .setPositiveButton(R.string.save, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        showSaveOptionDialog();
                    }
                })
                .setNegativeButton(R.string.leave, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .show();
    }

    private void showSaveOptionDialog() {
        new AlertDialog.Builder(this)
                .setTitle(R.string.dialog_save_option_title)
                .setMessage(R.string.dialog_save_option_message)
                .setPositiveButton(R.string.database, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        saveNote(new DbNote());
                    }
                })
                .setNegativeButton(R.string.device, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        saveNote(new LocalNote());
                    }
                })
                .show();
    }

    private void saveNote(INote note) {
        note.setColor(mSelectedColor);
        note.setTitle(edtTitle.getText().toString());
        note.setContent(edtContent.getText().toString());
        NotesProvider.Instance().save(this, note);
        finish();
    }

    private final SpectrumDialog.OnColorSelectedListener mColorSelectListener
            = new SpectrumDialog.OnColorSelectedListener() {
        @Override
        public void onColorSelected(boolean positiveResult, @ColorInt int color) {
            mSelectedColor = color;
        }
    };

}