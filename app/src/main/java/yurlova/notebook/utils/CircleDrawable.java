package yurlova.notebook.utils;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;

/**
 * Created by maestro123 on 5/23/17.
 */

public class CircleDrawable extends ColorDrawable {

    private final Paint mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    public CircleDrawable() {
    }

    public CircleDrawable(int color) {
        mPaint.setColor(color);
    }

    @Override
    public void draw(Canvas canvas) {
        final Rect bounds = getBounds();
        canvas.drawCircle(bounds.centerX(), bounds.centerY(), Math.min(bounds.width(), bounds.height()) >> 1, mPaint);
    }

    public void setColor(int color) {
        mPaint.setColor(color);
        invalidateSelf();
    }

}