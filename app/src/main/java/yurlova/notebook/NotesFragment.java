package yurlova.notebook;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import yurlova.notebook.core.INote;
import yurlova.notebook.core.NotesProvider;

public class NotesFragment extends Fragment implements LoaderManager.LoaderCallbacks<List<INote>> {

    private static final int DATA_LOADER_ID = 0;

    private ListView mList;
    private TextView txtEmpty;
    private NotesAdapter mAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_notes, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mList = (ListView) view.findViewById(R.id.list);
        txtEmpty = (TextView) view.findViewById(R.id.empty);
        mAdapter = new NotesAdapter(getContext());
        mList.setAdapter(mAdapter);
        mList.setOnItemClickListener(mItemClickListener);

        getLoaderManager().initLoader(DATA_LOADER_ID, null, this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        NotesProvider.Instance().removeOnDataChangeListener(mChangeListener);
    }

    @Override
    public Loader<List<INote>> onCreateLoader(int id, Bundle args) {
        return new NotesLoader(getContext());
    }

    @Override
    public void onLoadFinished(Loader<List<INote>> loader, List<INote> data) {
        mAdapter.update(data);
        verifyEmptyView();
        NotesProvider.Instance().addOnDataChangeListener(mChangeListener);
    }

    @Override
    public void onLoaderReset(Loader<List<INote>> loader) {
        //ignore
    }

    private void verifyEmptyView() {
        txtEmpty.setVisibility(mAdapter == null || mAdapter.getCount() == 0 ? View.VISIBLE : View.GONE);
    }

    private final NotesProvider.OnDataChangeListener mChangeListener = new NotesProvider.OnDataChangeListener() {
        @Override
        public void onDataChanged(final List<INote> items) {
            if (getView() != null) {
                getView().post(new Runnable() {
                    @Override
                    public void run() {
                        if (mAdapter != null) {
                            mAdapter.update(items);
                        }
                        verifyEmptyView();
                    }
                });
            }
        }
    };

    private final AdapterView.OnItemClickListener mItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            CreateNoteActivity.start(parent.getContext(), mAdapter.getItem(position).getKey());
        }
    };

    public static class NotesLoader extends AsyncTaskLoader<List<INote>> {

        public NotesLoader(Context context) {
            super(context);
        }

        @Override
        protected void onStartLoading() {
            super.onStartLoading();
            forceLoad();
        }

        @Override
        public List<INote> loadInBackground() {
            return NotesProvider.Instance().load(getContext());
        }
    }

}