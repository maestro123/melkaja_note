package yurlova.notebook;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import java.util.List;

import yurlova.notebook.core.INote;
import yurlova.notebook.core.NotesProvider;
import yurlova.notebook.core.model.DbNote;
import yurlova.notebook.utils.CircleDrawable;
import yurlova.notebook.utils.Utils;

public class NotesAdapter extends BaseAdapter {

    private Context mContext;
    private LayoutInflater mInflater;

    private List<INote> mNotes;

    public NotesAdapter(Context context) {
        mContext = context;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return mNotes != null ? mNotes.size() : 0;
    }

    @Override
    public INote getItem(int position) {
        return mNotes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ItemHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.note_item_view, parent, false);
            holder = new ItemHolder(convertView);
        } else {
            holder = (ItemHolder) convertView.getTag();
        }
        final INote note = mNotes.get(position);
        holder.setNote(note);
        return convertView;
    }

    public void update(List<INote> notes) {
        mNotes = notes;
        notifyDataSetChanged();
    }

    class ItemHolder {

        TextView txtDate;
        TextView txtTitle;
        TextView txtContent;
        ImageButton btnMore;
        ImageView mColorImage;
        CircleDrawable mColorDrawable;

        INote mNote;

        public ItemHolder(View view) {
            txtDate = (TextView) view.findViewById(R.id.date);
            txtTitle = (TextView) view.findViewById(R.id.title);
            txtContent = (TextView) view.findViewById(R.id.content);
            btnMore = (ImageButton) view.findViewById(R.id.btn_more);
            mColorImage = (ImageView) view.findViewById(R.id.color_icon);
            mColorDrawable = new CircleDrawable();
            mColorImage.setBackgroundDrawable(mColorDrawable);
            view.setTag(this);
            btnMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    final PopupMenu menu = new PopupMenu(v.getContext(), v);
                    menu.getMenuInflater().inflate(R.menu.item_menu, menu.getMenu());
                    menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.edit: {
                                    CreateNoteActivity.start(v.getContext(), mNote.getKey());
                                    return true;
                                }
                                case R.id.delete: {
                                    new AlertDialog.Builder(v.getContext())
                                            .setTitle(R.string.dialog_delete_confirm_title)
                                            .setMessage(R.string.dialog_delete_confirm_message)
                                            .setPositiveButton(R.string.cancel, new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    //ignore
                                                }
                                            })
                                            .setNegativeButton(R.string.delete, new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    NotesProvider.Instance().delete(v.getContext(), mNote);
                                                }
                                            })
                                            .show();
                                    return true;
                                }
                            }
                            return false;
                        }
                    });
                    menu.show();
                }
            });
        }

        public void setNote(INote note) {
            mNote = note;
            if (TextUtils.isEmpty(mNote.getTitle()) || TextUtils.isEmpty(mNote.getContent())) {
                txtTitle.setVisibility(View.GONE);
                txtContent.setText(TextUtils.isEmpty(mNote.getTitle()) ? mNote.getContent() : mNote.getTitle());
                txtContent.setTextAppearance(mContext, R.style.ItemTextStyleTitle_Single);
            } else {
                txtTitle.setTextAppearance(mContext, R.style.ItemTextStyleTitle);
                txtContent.setTextAppearance(mContext, R.style.ItemTextStyleContent);
                txtTitle.setText(note.getTitle());
                txtContent.setText(note.getContent());
                txtTitle.setVisibility(View.VISIBLE);
            }
            txtDate.setText(Utils.formatDate(note));
            mColorDrawable.setColor(note.getColor());
            mColorImage.setImageResource(note instanceof DbNote
                    ? R.drawable.ic_database
                    : R.drawable.ic_local_storage);
        }

    }

}