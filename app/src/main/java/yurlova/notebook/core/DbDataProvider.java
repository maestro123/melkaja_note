package yurlova.notebook.core;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

public class DbDataProvider extends ContentProvider {

    private static final String AUTHORITY = "yurlova.notebook.provider";
    private static final String BASE_PATH = "notes";

    private static final int TYPE_NOTES = 0;
    private static final int TYPE_NOTE = 1;

    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + BASE_PATH);
    public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + BASE_PATH;
    public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/feed";

    private DatabaseHelper mHelper;

    private static final UriMatcher sURIMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        sURIMatcher.addURI(AUTHORITY, BASE_PATH, TYPE_NOTES);
        sURIMatcher.addURI(AUTHORITY, BASE_PATH + "/#", TYPE_NOTE);
    }

    @Override
    public boolean onCreate() {
        mHelper = new DatabaseHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        final SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        queryBuilder.setTables(NotesTable.NAME);

        final int uriType = sURIMatcher.match(uri);
        switch (uriType) {
            case TYPE_NOTES: {
                break;
            }
            case TYPE_NOTE: {
                queryBuilder.appendWhere(NotesTable.ID
                        + "=" + uri.getLastPathSegment());
                break;
            }
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        final SQLiteDatabase database = mHelper.getReadableDatabase();
        final Cursor cursor = queryBuilder.query(database, projection, selection, selectionArgs, null, null, sortOrder);
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        final long id = performInsert(uri, values);
        return Uri.parse(BASE_PATH + "/" + id);
    }

    @Override
    public int bulkInsert(@NonNull Uri uri, @NonNull ContentValues[] values) {
        int insertedCount = 0;
        for (int i = 0; i < values.length; i++) {
            long id = performInsert(uri, values[i]);
            if (id != -1) {
                insertedCount++;
            }
        }
        return insertedCount;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        int uriType = sURIMatcher.match(uri);
        SQLiteDatabase sqlDB = mHelper.getWritableDatabase();
        int rowsDeleted = 0;
        switch (uriType) {
            case TYPE_NOTES:
                rowsDeleted = sqlDB.delete(NotesTable.NAME, selection,
                        selectionArgs);
                break;
            case TYPE_NOTE:
                String id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsDeleted = sqlDB.delete(
                            NotesTable.NAME,
                            NotesTable.ID + "=" + id,
                            null);
                } else {
                    rowsDeleted = sqlDB.delete(
                            NotesTable.NAME,
                            NotesTable.ID + "=" + id
                                    + " and " + selection,
                            selectionArgs);
                }
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return rowsDeleted;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        int uriType = sURIMatcher.match(uri);
        SQLiteDatabase sqlDB = mHelper.getWritableDatabase();
        int rowsUpdated = 0;
        switch (uriType) {
            case TYPE_NOTES:
                rowsUpdated = sqlDB.update(NotesTable.NAME,
                        values,
                        selection,
                        selectionArgs);
                break;
            case TYPE_NOTE:
                String id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsUpdated = sqlDB.update(NotesTable.NAME,
                            values,
                            NotesTable.ID + "=" + id,
                            null);
                } else {
                    rowsUpdated = sqlDB.update(NotesTable.NAME,
                            values,
                            NotesTable.ID + "=" + id
                                    + " and "
                                    + selection,
                            selectionArgs);
                }
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return rowsUpdated;
    }

    private long performInsert(Uri uri, ContentValues values) {
        int uriType = sURIMatcher.match(uri);
        SQLiteDatabase sqlDB = mHelper.getWritableDatabase();
        long id = 0;
        switch (uriType) {
            case TYPE_NOTES:
                id = sqlDB.insertWithOnConflict(NotesTable.NAME, null, values, SQLiteDatabase.CONFLICT_IGNORE);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return id;
    }

}