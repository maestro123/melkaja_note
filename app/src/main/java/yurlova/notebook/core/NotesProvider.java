package yurlova.notebook.core;

import android.content.Context;
import android.database.Cursor;

import java.io.File;
import java.io.FileFilter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import yurlova.notebook.core.model.DbNote;
import yurlova.notebook.core.model.LocalNote;
import yurlova.notebook.utils.Utils;

public class NotesProvider {

    private static volatile NotesProvider instance;

    public static synchronized NotesProvider Instance() {
        if (instance == null) {
            synchronized (NotesProvider.class) {
                if (instance == null) {
                    instance = new NotesProvider();
                }
            }
        }
        return instance;
    }

    private final Object mSyncLock = new Object();

    private HashMap<Serializable, INote> mNotes = new HashMap<>();
    private boolean isLoaded = false;

    public List<INote> load(Context context) {
        if (!isLoaded) {
            synchronized (mSyncLock) {
                final Cursor cursor = context.getContentResolver().query(DbDataProvider.CONTENT_URI, null, null, null, null);
                if (cursor != null) {
                    while (cursor.moveToNext()) {
                        DbNote note = new DbNote(cursor);
                        mNotes.put(note.getKey(), note);
                    }
                }
                File localDir = getLocalNotesStorage(context);
                for (File file : localDir.listFiles(sNotesFilter)) {
                    LocalNote note = LocalNote.Create(file);
                    if (note != null) {
                        mNotes.put(note.getKey(), note);
                    }
                }
                isLoaded = true;
                //TODO: notify?
            }
        }
        return getNotesList();
    }

    public File getLocalNotesStorage(Context context) {
        File file = new File(context.getFilesDir(), "notes");
        file.mkdirs();
        return file;
    }

    public List<INote> getNotesList() {
        synchronized (mSyncLock) {
            final ArrayList<INote> notes = new ArrayList<>(mNotes.values());
            Collections.sort(notes, NotesComparator);
            return notes;
        }
    }

    public void save(Context context, INote note) {
        synchronized (mSyncLock) {
            int saveResult = note.save(context);
            if (saveResult == INote.SAVE_ERROR) {
                return;
            }
            if (saveResult == INote.SAVE_NEW) {
                mNotes.put(note.getKey(), note);
            }
            notifyDataChanged();
        }
    }

    public void delete(Context context, INote note) {
        synchronized (mSyncLock) {
            if (note.delete(context) && mNotes.remove(note.getKey()) != null) {
                notifyDataChanged();
            }
        }
    }

    public INote getNote(Serializable key) {
        return mNotes.get(key);
    }

    private final ArrayList<OnDataChangeListener> mListeners = new ArrayList<>();

    public void addOnDataChangeListener(OnDataChangeListener listener) {
        mListeners.add(listener);
    }

    public void removeOnDataChangeListener(OnDataChangeListener listener) {
        mListeners.remove(listener);
    }

    private void notifyDataChanged() {
        synchronized (mSyncLock) {
            List<INote> notes = getNotesList();
            for (OnDataChangeListener listener : mListeners) {
                listener.onDataChanged(notes);
            }
        }
    }

    public interface OnDataChangeListener {
        void onDataChanged(List<INote> items);
    }

    private static final FileFilter sNotesFilter = new FileFilter() {
        @Override
        public boolean accept(File pathname) {
            return pathname.getName().endsWith(NOTE_EXTENSION);
        }
    };

    public static final String NOTE_EXTENSION = ".note";

    private static final Comparator<INote> NotesComparator = new Comparator<INote>() {
        @Override
        public int compare(INote o1, INote o2) {
            return Utils.compare(o2.getDate(), o1.getDate());
        }
    };

}