package yurlova.notebook.core;

public final class NotesTable extends DatabaseHelper.BaseTable {

    public static final String NAME = "tblFeeds";

    public static final int TITLE_INDEX = ID_INDEX + 1;
    public static final String TITLE = "title";

    public static final int CONTENT_INDEX = ID_INDEX + 2;
    public static final String CONTENT = "content";

    public static final int DATE_INDEX = ID_INDEX + 3;
    public static final String DATE = "date";

    public static final int COLOR_INDEX = ID_INDEX + 4;
    public static final String COLOR = "color";

    public static final String CREATE = "create table " + NAME + "("
            + ID + " integer primary key autoincrement,"
            + TITLE + " text not null,"
            + CONTENT + " text,"
            + DATE + " integer,"
            + COLOR + " integer"
            + ")";
}