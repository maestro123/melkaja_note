package yurlova.notebook.core;

public class Constants {

    public static final String NOTE = "note";

    public static final String TITLE = "title";
    public static final String CONTENT = "content";
    public static final String DATE = "date";
    public static final String COLOR = "color";

}
