package yurlova.notebook.core.model;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import java.io.Serializable;

import yurlova.notebook.core.DbDataProvider;
import yurlova.notebook.core.INote;
import yurlova.notebook.core.NotesTable;

public class DbNote extends INote {

    private long id;

    public DbNote() {
    }

    public DbNote(Cursor cursor) {
        id = cursor.getLong(NotesTable.ID_INDEX);
        setTitle(cursor.getString(NotesTable.TITLE_INDEX));
        setContent(cursor.getString(NotesTable.CONTENT_INDEX));
        setDate(cursor.getLong(NotesTable.DATE_INDEX));
        setColor(cursor.getInt(NotesTable.COLOR_INDEX));
    }

    @Override
    public Serializable getKey() {
        return id;
    }

    @Override
    protected int save(Context context) {
        ContentValues values = new ContentValues();
        values.put(NotesTable.TITLE, getTitle());
        values.put(NotesTable.CONTENT, getContent());
        values.put(NotesTable.DATE, getDate());
        values.put(NotesTable.COLOR, getColor());
        if (id != 0) {
            int count = context.getContentResolver().update(ContentUris.withAppendedId(DbDataProvider.CONTENT_URI, id), values, null, null);
            return count > 0 ? SAVE_UPDATE : SAVE_ERROR;
        } else {
            setDate(System.currentTimeMillis());
            Uri uri = context.getContentResolver().insert(DbDataProvider.CONTENT_URI, values);
            id = ContentUris.parseId(uri);
            return uri != null ? SAVE_NEW : SAVE_ERROR;
        }
    }

    @Override
    protected boolean delete(Context context) {
        int count = context.getContentResolver().delete(ContentUris.withAppendedId(DbDataProvider.CONTENT_URI, id), null, null);
        return count > 0;
    }
}