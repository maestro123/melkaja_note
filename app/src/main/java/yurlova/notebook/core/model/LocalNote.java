package yurlova.notebook.core.model;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;

import yurlova.notebook.core.Constants;
import yurlova.notebook.core.INote;
import yurlova.notebook.core.NotesProvider;
import yurlova.notebook.utils.Utils;

public class LocalNote extends INote {

    public static LocalNote Create(File file) {
        try {
            JSONObject jsonObject = new JSONObject(Utils.getStringFromFile(file));
            LocalNote note = new LocalNote(file);
            note.setTitle(jsonObject.optString(Constants.TITLE));
            note.setContent(jsonObject.optString(Constants.CONTENT));
            note.setDate(jsonObject.optLong(Constants.DATE));
            note.setColor(jsonObject.optInt(Constants.COLOR));
            return note;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private File mFile;

    public LocalNote() {
    }

    public LocalNote(File file) {
        mFile = file;
    }

    @Override
    public Serializable getKey() {
        return mFile.getName();
    }

    @Override
    protected int save(Context context) {
        int saveResult = SAVE_UPDATE;
        if (mFile == null) {
            mFile = new File(
                    NotesProvider.Instance().getLocalNotesStorage(context),
                    System.currentTimeMillis() + "." + NotesProvider.NOTE_EXTENSION);
            setDate(System.currentTimeMillis());
            saveResult = SAVE_NEW;
        }
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.TITLE, getTitle());
            jsonObject.put(Constants.CONTENT, getContent());
            jsonObject.put(Constants.DATE, getDate());
            jsonObject.put(Constants.COLOR, getColor());

            FileWriter writer = new FileWriter(mFile);
            writer.write(jsonObject.toString());
            writer.flush();
            writer.close();

            return saveResult;
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return SAVE_ERROR;
    }

    @Override
    protected boolean delete(Context context) {
        if (mFile != null) {
            return mFile.delete();
        }
        return false;
    }
}
