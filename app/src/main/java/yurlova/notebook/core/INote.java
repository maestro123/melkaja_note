package yurlova.notebook.core;

import android.content.Context;
import android.graphics.Color;

import java.io.Serializable;

public abstract class INote {

    public static int SAVE_NEW = 0;
    public static int SAVE_UPDATE = 1;
    public static int SAVE_ERROR = 2;

    private String title;
    private String content;
    private long date;
    private int color = Color.parseColor("#2196F3");

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public abstract Serializable getKey();

    protected abstract int save(Context context);

    protected abstract boolean delete(Context context);

}
